import React , { Component } from 'react';
// import { connect } from 'react-redux';
import LoginForm from '../components/LoginForm';
import Message from '../components/Message';
// import * as actions from '../store/actions/index';



class LoginAppBuilder extends Component {
    state = {
        email : '',
        password : '',
        loggedin : false,
        error : false,
        showMsg : true,
        validate : false,
        errorMsg :''
    }

    componentDidMount () {
    }


    handleSubmit = (event) => {
        event.preventDefault();
        let userData = {
            email : this.state.email,
            password : this.state.password
        };
        fetch('http://localhost:3000/user/sign-up',
            {
            method: 'POST',
            headers: {
              'Content-Type': 'application/json',
              'Access-Control-Allow-Origin': '*'
            },
            body : JSON.stringify(userData)
            }
        )
        .then(response => {
                response.json().then(data =>{
                    if(data.error){
                        this.setState({
                            ...this.state,
                            error : true,
                            errorMsg : data.message,
                            showMsg : true
                        });
                    }else{
                        this.setState({
                            ...this.state,
                            error : false,
                            errorMsg : data.message,
                            showMsg : true
                        });
                    }
                })
        })
        .catch(error => {
            console.log("=========error===========");
            console.log(error);
        });
    }

    handleSignIn = (event) => {
        event.preventDefault();
        if(this.state.email == '' && this.state.password == ''){
            this.setState({
                ...this.state,
                error : true,
                errorMsg : 'Please Provied Email ID and Password',
                showMsg : true
            });
        }else{
            let userData = {
                email : this.state.email,
                password : this.state.password
            };
            fetch('http://localhost:3000/user/sign-in',
                {
                method: 'POST',
                headers: {
                  'Content-Type': 'application/json',
                  'Access-Control-Allow-Origin': '*'
                },
                body : JSON.stringify(userData)
                }
            )
            .then(response => {
                    console.log("=====response=====");
                    console.log(response);
                    response.json().then(data =>{

                        if(data.error){
                            this.setState({
                                ...this.state,
                                error : true,
                                errorMsg : data.message,
                                showMsg : true
                            });
                        }else{
                            this.setState({
                                ...this.state,
                                error : false,
                                errorMsg : data.message,
                                showMsg : true,
                                loggedin : true
                            });
                            localStorage.setItem('login',JSON.stringify(
                            { login : true , token : data.token, email: this.state.email}));
                        }
                    })
            })
            .catch(error => {
                console.log("=========error===========");
                console.log(error);
            });
        }
    }

    handleChange = (event) => {
        event.preventDefault();
        const { type, value } = event.target;
        if(event.target.type === 'email') {
            this.setState({
                ...this.state,
                email : event.target.value
            });
        }
        if(event.target.type === 'password'){
            this.setState({
                ...this.state,
                password : event.target.value
            });
        }
    }

    handleMsg = () => {
        this.setState({
                ...this.state,
                showMsg : false,
                email : '',
                password : ''
        });
        document.getElementById("todo-form").reset();
        if(this.state.loggedin){
            this.props.history.replace( '/dashboard' );
        }
    }

    render(){
        let msg = (this.state.showMsg && this.state.errorMsg != '') ? (<Message error={this.state.error} show={this.state.showMsg} msg={this.state.errorMsg} setShow={this.handleMsg} />) : '';
        let data = {
                email : this.state.email,
                password : this.state.password
            }
        return (
            <React.Fragment>

            <LoginForm data={data} validate={this.state.errorMsg} handleSubmit={this.handleSubmit} handleChange={this.handleChange} handleSignIn={this.handleSignIn} />
            {msg}
            </React.Fragment>
        );
    }
}


const mapStateToProps = state => {
    return {
        email: state.task.email,
        password : state.task.password,
        error : state.task.error,
        loggedin : state.task.loggedin,
        errorMsg : state.task.errorMsg
    }
};

// const mapDispatchToProps = dispatch => {
//     return {
//         onSubmitSignUp: (task) => dispatch(actions.initSignTask(task))
//     };
// };


export default LoginAppBuilder;
