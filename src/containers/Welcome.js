import React , { Component } from 'react';
import { Redirect } from "react-router-dom";
// import { connect } from 'react-redux';
// import * as actions from '../store/actions/index';



class Welcome extends Component {
    state = {
        email : '',
        loggedin : false,
        token : null,
        error : false,
        msg : ''
    }

    componentWillMount () {
        let logdata = JSON.parse(localStorage.getItem('login'));
        if(logdata != null){
            this.setState({
                loggedin : logdata.login,
                email : logdata.email,
                token : logdata.token
            });
        }
    }

    componentDidMount () {
        fetch('http://localhost:3000/user',
                {
                method: 'POST',
                headers: {
                  'Content-Type': 'application/json',
                  'Authorization':this.state.token ,
                  'Access-Control-Allow-Origin': '*'
                }

                }
            )
            .then(response => {
                    response.json().then(data =>{
                        if(data.error){
                            this.setState({
                                ...this.state,
                                error : true,
                                loggedin : false
                            });
                        }else{
                            this.setState({
                                ...this.state,
                                error : false,
                                loggedin : true,
                                msg : data.message
                            });
                        }
                    })
            })
            .catch(error => {
                console.log("=========error===========");
                console.log(error);
            });
    }



    render(){

        let comp = this.state.loggedin ? (<div> {this.state.msg}  {this.state.email} ..!!</div>) : (<Redirect to="/" />);
        return (
            <React.Fragment>
             {comp}
            </React.Fragment>
        );
    }
}


const mapStateToProps = state => {
    return {
        name: state.todoTask.name,
        date : state.todoTask.date,
        id : state.todoTask.id,
        added : state.todoTask.added,
        edited : state.todoTask.edited,
        error: state.todoTask.error,
        loading: state.todoTask.loading
    }
};

// const mapDispatchToProps = dispatch => {
//     return {
//         onSubmitTask: (task) => dispatch(actions.initAddTask(task))
//     };
// };


export default Welcome;
