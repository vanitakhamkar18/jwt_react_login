import React , { Component } from 'react';
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Card from 'react-bootstrap/Card';
import ErrorBoundary from './ErrorBoundary';

class Layout extends Component {
    render(){
        return (
        <ErrorBoundary>
            <div className="wrapper">
                <Container className="text">
                    <Card>
                        <Card.Body>
                        {this.props.children}
                        </Card.Body>
                    </Card>
                </Container>
            </div>
        </ErrorBoundary>
        );
    }
}

export default Layout;
