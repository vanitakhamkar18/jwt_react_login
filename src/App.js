import React , { Component } from 'react';
import { BrowserRouter,Route, Switch } from 'react-router-dom';
import 'bootstrap/dist/css/bootstrap.min.css';
import './App.css';
import Layout from './hoc/Layout';
import LoginAppBuilder from './containers/LoginAppBuilder';
import Welcome from './containers/Welcome';

class App extends Component {
    render(){
        return (
            <BrowserRouter className="App">
                <Layout>
                    <Switch>
                        <Route path="/dashboard" component={Welcome} />
                        <Route path="/" exact component={LoginAppBuilder} />
                    </Switch>
                </Layout>
            </BrowserRouter>
        );
    }
}

export default App;
