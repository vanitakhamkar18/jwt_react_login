import React  from 'react';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Form from 'react-bootstrap/Form';
import Button from 'react-bootstrap/Button';

const LoginForm = (props) => {
    return(
        <Form id="todo-form" onSubmit={props.handleSubmit}>
        <Form.Group controlId="fromTodo">
            <Form.Label>Log In :</Form.Label>
          <Row>
            <Col>
              <Form.Control type="email"  required  defaultValue={typeof props.data.email ===  'undefined' ? '' : props.data.email} placeholder="Enter Email" onChange={props.handleChange} />


            </Col>
            <Col>
              <Form.Control type="password" required  defaultValue={typeof props.data.password ===  'undefined' ? '' : props.data.password} onChange={props.handleChange} placeholder="Enter Passward" />
            </Col>
          </Row>
        </Form.Group>
          <Row>
            <Button variant="primary" type="submit" className="ml-3">
                Sign Up
            </Button>
            <Button variant="info" type="button" className="ml-3" onClick={props.handleSignIn}>
                Sign In
            </Button>
          </Row>
        </Form>
    );
}

export default LoginForm;
